function createCell(data, class1, class2, class3) {
	const tableCell = document.createElement('th');
	tableCell.textContent = `${data}`;
	tableCell.classList.add(`${class1}`);
	tableCell.classList.add(`${class2}`);
	tableCell.classList.add(`${class3}`);
	return tableCell;
}

function createRow(dataObject) {
	const tableRow = document.createElement('tr');
	tableRow.classList.add('table');
	tableRow.classList.add('table__tableRow');
	tableRow.setAttribute('id', `${dataObject.id}`);
	tableRow.setAttribute('onclick', 'insertToEditor(event)');
	tableRow.appendChild(createCell(dataObject.id, 'table', 'table__tableCell', "table__tableCell-number"));
	tableRow.appendChild(createCell(dataObject.firstName, 'table', 'table__tableCell'));
	tableRow.appendChild(createCell(dataObject.lastName, 'table', 'table__tableCell'));
	tableRow.appendChild(createCell(dataObject.age, 'table', 'table__tableCell', "table__tableCell-number"));
	return tableRow;
}

function drawTable(dataArray, table) {
	dataArray.forEach(dataObject => table.append(createRow(dataObject)));
	return table;
}


function read(row) {
	const cells = row.getElementsByTagName('th');
	const data = {
		id: cells[0].textContent,
		firstName: cells[1].textContent,
		lastName: cells[2].textContent,
		age: cells[3].textContent,
	};
	return data;
}

function setRow(id, firstName, secondName, age, row) {
	const cells = row.getElementsByTagName('th');
	cells[0].textContent = id;
	cells[1].textContent = firstName;
	cells[2].textContent = secondName;
	cells[3].textContent = age;
}

function getData(version, action, id, firstName, lastName, age) {
	if (id.value && firstName.value && lastName.value && age.value) {
		const data = {
			version: version,
			action: action,
			id: id.value,
			firstName: firstName.value,
			lastName: lastName.value,
			age: age.value,
		};
		return data;
	} else {
		alert('Input all data please!');
		return false;
	}
}

function create() {
	const body = getData(version[0], 'create', id, firstName, lastName, age);
	sendRequest('POST', requestURL, body)
		.then(data => {
			if (checkData(data)) {
				table.append(createRow(data.person));
			} else alert(data)
		})
		.catch(err => console.log(err));
}

function checkData(data) {
	if (typeof (data) === 'string')
		return false;
	else return true;
}

function update() {
	const body = getData(version[0], 'update', id, firstName, lastName, age);
	sendRequest('POST', requestURL, body)
		.then(data => {
			if (checkData(data)) {
				const rows = table.children;
				for (let index = 0; index < rows.length; index++) {
					console.log(data.person);
					const person = read(rows[index]);
					if (person.id == data.person.id) {
						setRow(data.person.id, data.person.firstName, data.person.lastName, data.person.age, rows[index]);
						break;
					}
				}
			} else alert(data)
		})
		.catch(err => console.log(err));
}

function deleteRow() {
	const body = getData(version[0], 'delete', id, firstName, lastName, age);
	sendRequest('POST', requestURL, body)
		.then(data => {
			if (checkData(data)) {
				const rows = table.children;
				for (let index = 0; index < rows.length; index++) {
					const person = read(rows[index]);
					if (person.id == data.person.id) {
						 rows[index].remove()
						break;
					}
				}
			} else alert(data)
		})
		.catch(err => console.log(err));
}

function clearNode(node) {
	const numberOfChildren = node.children.length;
	for (let index = 0; index < numberOfChildren; index++) {
		node.lastChild.remove();
	}
}

function insertToEditor(event) {
	const target = event.target.parentNode;
	const data = read(target);
	id.value = data.id;
	firstName.value = data.firstName;
	lastName.value = data.lastName;
	age.value = data.age;
}

function clearData() {
	id.value = '';
	firstName.value = '';
	lastName.value = '';
	age.value = '';
}
