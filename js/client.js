const requestURL = 'http://127.0.0.1:3000';
// const requestURL = 'https://jsonplaceholder.typicode.com/users';
//
// function sendRequest() {
// 	const xhr = new XMLHttpRequest();
// 	xhr.open('GET', requestURL);
// 	// xhr.setRequestHeader('Content-Type', 'text/json');
// 	// xhr.setRequestHeader('Access-Control-Allow-Origin','*');
// 	xhr.send();
// 	xhr.addEventListener('readystatechange', function () {
// 		if (xhr.readyState === 4 && xhr.status === 200) {
// 			let data = JSON.parse(xhr.response);
// 			console.log(data);
// 			drawTable(data, table);
// 		} else {
// 			// console.error('something wrong');
// 			console.log(xhr.status);
// 			console.log(xhr.readyState);
// 		}
// 	});
// }
//
// sendRequest();

function sendRequest(method, url, body = null) {
	return new Promise ((resolve, reject) => {
		const xhr = new XMLHttpRequest();
	
		xhr.open(method, url);
		
		xhr.responseType = 'json';
		xhr.setRequestHeader('Content-Type', 'text/json');
		xhr.onload = () => {
			if (xhr.status >= 400) {
				reject(xhr.response)
			} else {
				resolve(xhr.response);
			}
		};
	
		xhr.onerror = () => {
			reject(xhr.response)
		};
		xhr.send(JSON.stringify(body));
	})
}

// sendRequest('GET', requestURL)
// 	.then(data => drawTable(data, table))
// 	.catch(err => console.log(err));

const body = {
	id: 6,
	firstName: 'Volodymyr',
	lastName: 'Oleksandrovych',
	age: 42,
};

sendRequest('POST', requestURL, body)
	.then(data => drawTable(data,table))
	.catch(err => console.log(err));