const requestURL = 'http://127.0.0.1:3000';

function sendRequest(method, url, body = null) {
	const headers = {
		'content-Type': 'text/json',
	};
	
	if (method === 'POST') {
		return fetch(url, {
			method: method,
			body: JSON.stringify(body),
			headers: headers,
		}).then(response => {
			if (response.ok) {
				return response.json();
			}
			return response.json().then(error => {
				const err = new Error('something wrong!');
				err.data = error;
				throw err;
			});
		})
	} else if (method === 'GET'){
		return fetch(url).then(response => {
			if (response.ok) {
				return response.json();
			}
			return response.json().then(error => {
				const err = new Error('something wrong!');
				err.data = error;
				throw err;
			})
		})
	}
}