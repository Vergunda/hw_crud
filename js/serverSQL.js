const http = require('http');
const mysql = require('mysql');

const mySQLConfig = {
	connectionLimit: 10,
	host: 'localhost',
	user: 'root',
	password: '',
	// database: 'hw_uidb',
	database: 'mySQL'
};

connection = mysql.createConnection(mySQLConfig);

const server = http.createServer();
server.on('request', (request, response) => {
	request.setEncoding('utf8');
	console.log(request.method);
	if (request.method === 'POST') {
		request.on('data', (chunk) => {
			const requestData = JSON.parse(chunk);
			const person = {
				id: Number(requestData.id),
				firstName: requestData.firstName,
				lastName: requestData.lastName,
				age: Number(requestData.age),
			};
			switch (requestData.action) {
				case 'onloadGet':
					connection.query('select * from persons', function (error, results, fields) {
						const dataArray = JSON.stringify(results);
						const responseData = {
							version: 1,
							action: 'onLoadGet',
							dataArray: JSON.parse(dataArray),
						};
						sendResponse(response, responseData);
						if (error) throw error;
					});
					break;
				case 'create':
					connection.query('select id from persons', function (error, results, fields) {
						const idArray = results;
						if (checkId(person.id, idArray)) {
							console.log(person);
							connection.query('INSERT INTO persons SET ?', person, function (error, results, fields) {
								console.log(error);
								const responseData = {
									version: 2,
									action: 'create',
									person: person,
								};
								sendResponse(response, responseData);
								if (error) throw error;
							});

						} else sendResponse(response, 'Id already exist!');
					});
					break;
				case 'update':
					connection.query('select id from persons', function (error, results, fields) {
						const idArray = results;
						if (checkId(person.id, idArray)) {
							sendResponse(response, 'Id does not exist!');
						} else {
							console.log(person);
							connection.query(`UPDATE persons SET firstName = ?, lastName = ?, age = ? where id = ?`, [person.firstName, person.lastName, person.age, person.id], function (error, results, fields) {
								// console.log(error);
								const responseData = {
									version: 2,
									action: 'update',
									person: person,
								};
								sendResponse(response, responseData);
								if (error) throw error;
							});
						}
					});
					break;
				case 'delete':
					connection.query('select id from persons', function (error, results, fields) {
						const idArray = results;
						if (checkId(person.id, idArray)) {
							sendResponse(response, 'Id does not exist!');
						} else {
							console.log(person);
							connection.query(`DELETE FROM persons where id = ?`, [person.id], function (error, results, fields) {
								// console.log(error);
								const responseData = {
									version: 2,
									action: 'delete',
									person: person,
								};
								sendResponse(response, responseData);
								if (error) throw error;
							});
						}
					});
			}
		});
	} else {
		sendResponse(response);
	}
});


server.listen(3000, 'localhost', () => console.log('Server is working!'));

function create(dataArray, table) {
	const newData = getData(id, firstName, lastName, age);
	if (checkId(newData.id, dataArray)) {
		dataArray.push(newData);
		table.append(createRow(newData));
		return dataArray;
	} else {
		alert('Id is already exist!')
	}
}

function checkId(id, dataArray) {
	let isIdExist = 0;
	if (typeof (Number(id)) === 'number') {
		dataArray.forEach(dataObject => {
				if (Number(dataObject.id) === Number(id)) {
					isIdExist = 1;
				}
			}
		);
		if (!isIdExist) {
			return true;
		} else return false;
	} else return false
}

function sendResponse(response, data) {
	response.setHeader('Access-Control-Allow-Origin', '*');
	response.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
	response.setHeader('Access-Control-Allow-Headers', 'Content-Type');
	response.writeHead(200, {'Content-Type': 'text/json'});
	response.end(JSON.stringify(data));
}

//
// function mySQLConnection() {
// 	const connection = mysql.createConnection(mySQLConfig);
//
// 	connection.connect(function (err) {
// 		if (err) {
// 			console.error('error connecting: ' + err.stack);
// 			return;
// 		}
//
// 		console.log('connected as id ' + connection.threadId);
// 	});
// 	console.log('client is running');
//
// 	connection.query('SELECT * FROM PERSONS', function (error, results, fields) {
// 		const dataArray = JSON.stringify(results);
// 		const responseData = {
// 			version: 1,
// 			action: 'onLoadGet',
// 			dataArray: JSON.parse(dataArray),
// 		};
// 		console.log(responseData);
// 		// // console.log(response)
// 		// sendResponse(response, responseData);
// 		if (error) throw error;
// 		console.log('The solution is: ', results[0].solution);
// 		return responseData
// 	});
//
// 	connection.end();
// }

// console.log(response);
// console.log(response.method);
// console.log(response.headers);

