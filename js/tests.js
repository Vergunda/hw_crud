mocha.setup('bdd');

describe('CRUD tests', () => {
	
	describe('createCell function', () => {
		
		const testData = [
			{
				value: '1',
				class1: 'table',
				class2: 'table__tableCell',
				class3: 'table__tableCell-number',
			},
			{
				value: 'Leonid',
				class1: 'table',
				class2: 'table__tableCell',
			},
			{
				value: 'Danylovich',
				class1: 'table',
				class2: 'table__tableCell',
			},
			{
				value: '81',
				class1: 'table',
				class2: 'table__tableCell',
				class3: 'table__tableCell-number',
			},
		];
		
		testData.forEach(data => {
			
			const {value, class1, class2, class3} = data;
			
			it(`should createCell with ${value}, ${class1}, ${class2}, ${class3}`, () => {
				
				const expected = document.createElement('th');
				expected.innerHTML = `${value}`;
				expected.classList.add(`${class1}`);
				expected.classList.add(`${class2}`);
				expected.classList.add(`${class3}`);
				
				const actual = createCell(value, class1, class2, class3);
				
				assert.deepEqual(actual, expected);
			});
		});
	});
	
	describe('createRow function', () => {
		
		const testData = [
			{
				id: '1',
				firstName: 'Leonid',
				lastName: 'Makarovich',
				age: '89',
			},
			{
				id: '2',
				firstName: 'Leonid',
				lastName: 'Danylovich',
				age: '81',
			},
			{
				id: '3',
				firstName: 'Victor',
				lastName: 'Andrijovych',
				age: '65',
			},
			{
				id: '4',
				firstName: 'Victor',
				lastName: 'Fedorovych',
				age: '69',
			},
			{
				id: '5',
				firstName: 'Petro',
				lastName: 'Oleksiyovych',
				age: '54',
			},
		
		];
		
		testData.forEach(data => {
			
			const {id, firstName, lastName, age} = data;
			
			it(`should create row with ${id}, ${firstName}, ${lastName}, ${age}`, () => {
				const expected = document.createElement('tr');
				expected.classList.add('table');
				expected.classList.add('table__tableRow');
				expected.setAttribute('id', `${data.id}`);
				expected.setAttribute('onclick', 'insertToEditor(event)');
				expected.appendChild(createCell(id, 'table', 'table__tableCell', "table__tableCell-number"));
				expected.appendChild(createCell(firstName, 'table', 'table__tableCell',));
				expected.appendChild(createCell(lastName, 'table', 'table__tableCell'));
				expected.appendChild(createCell(age, 'table', 'table__tableCell', "table__tableCell-number"));
				
				const actual = createRow(data);
				
				assert.deepEqual(actual.outerHTML, expected.outerHTML);
			});
		});
	});
	
	describe('drawTable function', () => {
		
		it(`should create table row with 1 rows`, () => {
			const testData = [
				{
					id: '1',
					firstName: 'Leonid',
					lastName: 'Makarovich',
					age: '89',
				},
			];
			const expectedTable = document.createElement('table');
			const table = document.createElement('table');
			testData.forEach(data => {
				const expected = document.createElement('tr');
				expected.classList.add('table');
				expected.classList.add('table__tableRow');
				expected.setAttribute('id', `${data.id}`)
				expected.setAttribute('onclick', 'insertToEditor(event)');
				expected.appendChild(createCell(data.id, 'table', 'table__tableCell', "table__tableCell-number"));
				expected.appendChild(createCell(data.firstName, 'table', 'table__tableCell',));
				expected.appendChild(createCell(data.lastName, 'table', 'table__tableCell'));
				expected.appendChild(createCell(data.age, 'table', 'table__tableCell', "table__tableCell-number"));
				expectedTable.append(expected)
			});
			
			const actualTable = drawTable(testData, table);
			
			assert.deepEqual(actualTable.outerHTML, expectedTable.outerHTML);
		});
		
		it(`should create table row with 2 rows`, () => {
			const testData = [
				{
					id: '1',
					firstName: 'Leonid',
					lastName: 'Makarovich',
					age: '89',
				},
				{
					id: '2',
					firstName: 'Leonid',
					lastName: 'Danylovich',
					age: '81',
				},
			];
			const expectedTable = document.createElement('table');
			const table = document.createElement('table');
			testData.forEach(data => {
				const expected = document.createElement('tr');
				expected.classList.add('table');
				expected.classList.add('table__tableRow');
				expected.setAttribute('id', `${data.id}`)
				expected.setAttribute('onclick', 'insertToEditor(event)');
				expected.appendChild(createCell(data.id, 'table', 'table__tableCell', "table__tableCell-number"));
				expected.appendChild(createCell(data.firstName, 'table', 'table__tableCell',));
				expected.appendChild(createCell(data.lastName, 'table', 'table__tableCell'));
				expected.appendChild(createCell(data.age, 'table', 'table__tableCell', "table__tableCell-number"));
				expectedTable.append(expected)
			});
			
			
			const actualTable = drawTable(testData, table);
			
			assert.deepEqual(actualTable.outerHTML, expectedTable.outerHTML);
		});
		
		it(`should create table row with 5 rows`, () => {
			const testData = [
				{
					id: '1',
					firstName: 'Leonid',
					lastName: 'Makarovich',
					age: '89',
				},
				{
					id: '2',
					firstName: 'Leonid',
					lastName: 'Danylovich',
					age: '81',
				},
				{
					id: '3',
					firstName: 'Victor',
					lastName: 'Andrijovych',
					age: '65',
				},
				{
					id: '4',
					firstName: 'Victor',
					lastName: 'Fedorovych',
					age: '69',
				},
				{
					id: '5',
					firstName: 'Petro',
					lastName: 'Oleksiyovych',
					age: '54',
				},
			
			];
			const expectedTable = document.createElement('table');
			const table = document.createElement('table');
			testData.forEach(data => {
				const expected = document.createElement('tr');
				expected.classList.add('table');
				expected.classList.add('table__tableRow');
				expected.setAttribute('id', `${data.id}`)
				expected.setAttribute('onclick', 'insertToEditor(event)');
				expected.appendChild(createCell(data.id, 'table', 'table__tableCell', "table__tableCell-number"));
				expected.appendChild(createCell(data.firstName, 'table', 'table__tableCell',));
				expected.appendChild(createCell(data.lastName, 'table', 'table__tableCell'));
				expected.appendChild(createCell(data.age, 'table', 'table__tableCell', "table__tableCell-number"));
				expectedTable.append(expected)
			});
			
			
			const actualTable = drawTable(testData, table);
			
			assert.deepEqual(actualTable.outerHTML, expectedTable.outerHTML);
		});
	});
	
	describe('read function', () => {
		it('Should get data from row and set it to form', () => {
			const testData =
				{
					id: '1',
					firstName: 'Leonid',
					lastName: 'Makarovich',
					age: '89',
				};
			const row = document.createElement('tr');
			const cellId = document.createElement('th');
			const cellFirstName = document.createElement('th');
			const cellLastName = document.createElement('th');
			const cellAge = document.createElement('th');
			cellId.textContent = testData.id;
			cellFirstName.textContent = testData.firstName;
			cellLastName.textContent = testData.lastName;
			cellAge.textContent = testData.age;
			row.appendChild(cellId);
			row.appendChild(cellFirstName);
			row.appendChild(cellLastName);
			row.appendChild(cellAge);
			
			const actual = read(row);
			
			assert.deepEqual(actual, testData);
		});
	});
	
	describe('getData test function', () => {
		it('Should getData from input forms and return object', () => {
			const data =
				{
					id: '1',
					firstName: 'Leonid',
					lastName: 'Makarovich',
					age: '89',
				};
			const id = document.createElement('input');
			const firstName = document.createElement('input');
			const lastName = document.createElement('input');
			const age = document.createElement('input');
			id.value = data.id;
			firstName.value = data.firstName;
			lastName.value = data.lastName;
			age.value = data.age;
			
			const actual = getData(id, firstName, lastName, age);
			
			assert.deepEqual(actual, data);
		});
		let sandbox;
		
		before(() => {
			sandbox = sinon.createSandbox();
		});
		
		afterEach(() => {
			sandbox.restore();
		});
		it('Should getData from input forms and return object', () => {
			const data =
				{
					id: '',
					firstName: 'Leonid',
					lastName: 'Makarovich',
					age: '89',
				};
			const id = document.createElement('input');
			const firstName = document.createElement('input');
			const lastName = document.createElement('input');
			const age = document.createElement('input');
			const stub = sandbox.stub(window, 'alert')
			id.value = data.id;
			firstName.value = data.firstName;
			lastName.value = data.lastName;
			age.value = data.age;
			
			const actual = getData(id, firstName, lastName, age);
			
			assert.isFalse(actual);
			assert(stub.withArgs('Input all data please!').calledOnce);
		});
		it('Should getData return false and call alert, when some input is empty', () => {
			const data =
				{
					id: '1',
					firstName: '',
					lastName: 'Makarovich',
					age: '89',
				};
			const id = document.createElement('input');
			const firstName = document.createElement('input');
			const lastName = document.createElement('input');
			const age = document.createElement('input');
			const stub = sandbox.stub(window, 'alert')
			id.value = data.id;
			firstName.value = data.firstName;
			lastName.value = data.lastName;
			age.value = data.age;
			
			const actual = getData(id, firstName, lastName, age);
			
			assert.isFalse(actual);
			assert(stub.withArgs('Input all data please!').calledOnce);
		});
		it('Should getData return false and call alert, when some input is empty', () => {
			const data =
				{
					id: '1',
					firstName: 'Leonid',
					lastName: '',
					age: '89',
				};
			const id = document.createElement('input');
			const firstName = document.createElement('input');
			const lastName = document.createElement('input');
			const age = document.createElement('input');
			const stub = sandbox.stub(window, 'alert')
			id.value = data.id;
			firstName.value = data.firstName;
			lastName.value = data.lastName;
			age.value = data.age;
			
			const actual = getData(id, firstName, lastName, age);
			
			assert.isFalse(actual);
			assert(stub.withArgs('Input all data please!').calledOnce);
		});
		it('Should getData return false and call alert, when some input is empty', () => {
			const data =
				{
					id: '1',
					firstName: 'Leonid',
					lastName: 'Makarovich',
					age: '',
				};
			const id = document.createElement('input');
			const firstName = document.createElement('input');
			const lastName = document.createElement('input');
			const age = document.createElement('input');
			const stub = sandbox.stub(window, 'alert')
			id.value = data.id;
			firstName.value = data.firstName;
			lastName.value = data.lastName;
			age.value = data.age;
			
			const actual = getData(id, firstName, lastName, age);
			
			assert.isFalse(actual);
			assert(stub.withArgs('Input all data please!').calledOnce);
		});
	});
	
	describe('checkId function test', () => {
		
		let dataArray = [
			{
				id: '1',
				firstName: 'Leonid',
				lastName: 'Makarovich',
				age: '89',
			},
			{
				id: '2',
				firstName: 'Leonid',
				lastName: 'Danylovich',
				age: '81',
			},
			{
				id: '3',
				firstName: 'Victor',
				lastName: 'Andrijovych',
				age: '65',
			},
			{
				id: '4',
				firstName: 'Victor',
				lastName: 'Fedorovych',
				age: '69',
			},
			{
				id: '5',
				firstName: 'Petro',
				lastName: 'Oleksiyovych',
				age: '54',
			},
		];
		it('Should return true if checkId passed successfully', () => {
			const id = '1';
			
			const actual = checkId(id, dataArray);
			console.log(actual);
			
			assert.isFalse(actual);
		});
		it('Should return true if checkId passed successfully', () => {
			const id = 2;
			
			const actual = checkId(id, dataArray);
			console.log(actual);
			
			assert.isFalse(actual);
		});
		it('Should return true if checkId passed successfully', () => {
			const id = '6';
			
			const actual = checkId(id, dataArray);
			console.log(actual);
			
			assert.isTrue(actual);
		});
		it('Should return true if checkId passed successfully', () => {
			const id = 6;
			
			const actual = checkId(id, dataArray);
			console.log(actual);
			
			assert.isTrue(actual);
		});
	});
	
	describe('clearNode() function test', () => {
		
		it('should clear all children nodes', () => {
			const node = document.createElement('table');
			const row1 = document.createElement('tr');
			const row2 = document.createElement('tr');
			const row3 = document.createElement('tr');
			const row4 = document.createElement('tr');
			node.append(row1);
			node.append(row2);
			node.append(row3);
			node.append(row4);
			
			clearNode(node);
			
			assert.strictEqual(node.innerHTML, '');
		});
		
		it('should clear all children nodes', () => {
			const node = document.createElement('table');
			const row1 = document.createElement('tr');
			node.append(row1);
			
			clearNode(node);
			
			assert.strictEqual(node.innerHTML, '');
		})
		it('should clear all children nodes', () => {
			const node = document.createElement('table');
			const row1 = document.createElement('tr');
			const row2 = document.createElement('tr');
			node.append(row1);
			node.append(row2);
			
			clearNode(node);
			
			assert.strictEqual(node.innerHTML, '');
		});
	});
	
	describe('create function test', () => {
		let dataArray = [
			{
				id: '1',
				firstName: 'Leonid',
				lastName: 'Makarovich',
				age: '89',
			},
			{
				id: '2',
				firstName: 'Leonid',
				lastName: 'Danylovich',
				age: '81',
			},
			{
				id: '3',
				firstName: 'Victor',
				lastName: 'Andrijovych',
				age: '65',
			},
			{
				id: '4',
				firstName: 'Victor',
				lastName: 'Fedorovych',
				age: '69',
			},
			{
				id: '5',
				firstName: 'Petro',
				lastName: 'Oleksiyovych',
				age: '54',
			},
		];
		it('should create new row in table? id valid input data', () => {
			const table = document.createElement('table');
			dataArray.forEach(data => {
				const row = document.createElement('tr');
				
			});
			const row = document.createElement('tr');
			
		
		
		
		});
	});
	
	describe('update function test', () => {
		let dataArray = [
			{
				id: '1',
				firstName: 'Leonid',
				lastName: 'Makarovich',
				age: '89',
			},
			{
				id: '2',
				firstName: 'Leonid',
				lastName: 'Danylovich',
				age: '81',
			},
			{
				id: '3',
				firstName: 'Victor',
				lastName: 'Andrijovych',
				age: '65',
			},
			{
				id: '4',
				firstName: 'Victor',
				lastName: 'Fedorovych',
				age: '69',
			},
			{
				id: '5',
				firstName: 'Petro',
				lastName: 'Oleksiyovych',
				age: '54',
			},
		];
		it('should update existing row in table, id valid input data', () => {
		
		
		})
	});
	
	describe('delete function test', () => {
		let dataArray = [
			{
				id: '1',
				firstName: 'Leonid',
				lastName: 'Makarovich',
				age: '89',
			},
			{
				id: '2',
				firstName: 'Leonid',
				lastName: 'Danylovich',
				age: '81',
			},
			{
				id: '3',
				firstName: 'Victor',
				lastName: 'Andrijovych',
				age: '65',
			},
			{
				id: '4',
				firstName: 'Victor',
				lastName: 'Fedorovych',
				age: '69',
			},
			{
				id: '5',
				firstName: 'Petro',
				lastName: 'Oleksiyovych',
				age: '54',
			},
		];
		it('should update existing row in table, id valid input data', () => {
		
		
		})
	})
});

mocha.run();

// describe('insertToEditor function', () => {
// 	let sandbox;
//
// 	before(() => {
// 		sandbox = sinon.createSandbox();
// 	});
//
// 	afterEach(() => {
// 		sandbox.restore();
// 	});
//
// 	it('Should call function read() once with target row', () => {
// 		const data =
// 			{
// 				id: '1',
// 				firstName: 'Leonid',
// 				lastName: 'Makarovich',
// 				age: '89',
// 			};
// 		const row = document.createElement('tr');
// 		const stub = sandbox.stub(window, 'read');
// 		const event = {
// 				target: {
// 					parentElement: row,
// 				}
// 			};
// 		const id = document.createElement('input');
// 		const firstName = document.createElement('input');
// 		const lastName = document.createElement('input');
// 		const age = document.createElement('input');
// 		id.value = data.id;
// 		firstName.value = data.firstName;
// 		lastName.value = data.lastName;
// 		age.value = data.age;
//
// 		insertToEditor(event);
//
// 		assert(stub.calledOnce)
// 	});
//
// 	// it('Should insert data from dataObject to input form', () => {
// 	// 	const stub = sandbox.stub(window, 'read'),
// 	// 		event = {
// 	// 			target: {
// 	// 				dataset: {
// 	// 					show: ''
// 	// 				}
// 	// 			}
// 	// 		};
// 	// 	const testData =
// 	// 		{
// 	// 			id: '1',
// 	// 			firstName: 'Leonid',
// 	// 			lastName: 'Makarovich',
// 	// 			age: '89',
// 	// 		};
// 	// 	const dataObject = {
// 	// 		id: id.value,
// 	// 		firstName: firstName.value,
// 	// 		lastName: lastName.value,
// 	// 		age: age.value,
// 	// 	};
// 		const inputId = document.createElement('input');
// 		const inputFirstName = document.createElement('input');
// 		const inputLastName = document.createElement('input');
// 		const inputAge = document.createElement('input');
// 	// 	const row = document.createElement('tr');
// 	// 	const cellId = document.createElement('th');
// 	// 	const cellFirstName = document.createElement('th');
// 	// 	const cellLastName = document.createElement('th');
// 	// 	const cellAge = document.createElement('th');
// 	// 	cellId.textContent = testData.id;
// 	// 	cellFirstName.textContent = testData.firstName;
// 	// 	cellLastName.textContent = testData.lastName;
// 	// 	cellAge.textContent = testData.age;
// 	// 	row.appendChild(cellId);
// 	// 	row.appendChild(cellFirstName);
// 	// 	row.appendChild(cellLastName);
// 	// 	row.appendChild(cellAge);
// 	//
// 	//
// 	//
// 	// })
// })
// describe('getData function', () => {
//
// 	afterEach(() => {
// 		document.getElementById('testDiv').innerHTML = null;
// 	});
// 	const testData = [
// 		{
// 			valueOfId: '1',
// 			valueOfFirstName: 'Leonid',
// 			valueOfLastName: 'Makarovich',
// 			valueOfAge: '89',
// 		},
// 		{
// 			valueOfId: '2',
// 			valueOfFirstName: 'Leonid',
// 			valueOfLastName: 'Danylovich',
// 			valueOfAge: '81',
// 		},
// 		{
// 			valueOfId: '3',
// 			valueOfFirstName: 'Victor',
// 			valueOfLastName: 'Andrijovych',
// 			valueOfAge: '65',
// 		},
// 		{
// 			valueOfId: '4',
// 			valueOfFirstName: 'Victor',
// 			valueOfLastName: 'Fedorovych',
// 			valueOfAge: '69',
// 		},
// 		{
// 			valueOfId: '5',
// 			valueOfFirstName: 'Petro',
// 			valueOfLastName: 'Oleksiyovych',
// 			valueOfAge: '54',
// 		},
//
// 	];
//
// 	testData.forEach(data => {
//
// 		const {valueOfId, valueOfFirstName, valueOfLastName, valueOfAge} = data;
//
// 		it(`should return get correct data from input value`, () => {
// 			const testId = document.createElement('input');
// 			testId.setAttribute("id", "id");
// 			const testFirstName = document.createElement('input');
// 			testFirstName.setAttribute("id", "firstName");
// 			const testLastName = document.createElement('input');
// 			testLastName.setAttribute("id", "lastName");
// 			const testAge = document.createElement('input');
// 			testAge.setAttribute("id", "age");
// 			testId.value = valueOfId;
// 			testFirstName.value = valueOfFirstName;
// 			testLastName.value = valueOfLastName;
// 			testAge.value = valueOfAge;
// 			document.getElementById('testDiv').append(testId);
// 			document.getElementById('testDiv').append(testFirstName);
// 			document.getElementById('testDiv').append(testLastName);
// 			document.getElementById('testDiv').append(testAge);
//
// 			getData();
//
// 			assert.strictEqual(document.getElementById('id').value, valueOfId);
// 			assert.strictEqual(document.getElementById('firstName').value, valueOfFirstName)
// 			assert.strictEqual(document.getElementById('lastName').value, valueOfLastName)
// 			assert.strictEqual(document.getElementById('age').value, valueOfAge)
// 		});
// 	});
// });


//
// describe('Save function', () => {
//
// 	const testData = [
// 		{
// 			id: '1',
// 			firstName: 'Leonid',
// 			lastName: 'Makarovich',
// 			age: '89',
// 		},
// 		{
// 			id: '2',
// 			firstName: 'Leonid',
// 			lastName: 'Danylovich',
// 			age: '81',
// 		},
// 		{
// 			id: '3',
// 			firstName: 'Victor',
// 			lastName: 'Andrijovych',
// 			age: '65',
// 		},
// 		{
// 			id: '4',
// 			firstName: 'Victor',
// 			lastName: 'Fedorovych',
// 			age: '69',
// 		},
// 		{
// 			id: '5',
// 			firstName: 'Petro',
// 			lastName: 'Oleksiyovych',
// 			age: '54',
// 		},
//
// 	];
//
// 		let {id, firstName, lastName, age} = testData[0];
//
// 		it(`should create row with ${id}, ${firstName}, ${lastName}, ${age}`, () => {
// 			const testId = document.createElement('input');
// 			testId.setAttribute("id", "id");
// 			testId.value = id;
// 			const testFirstName = document.createElement('input');
// 			testFirstName.setAttribute("id", "firstName");
// 			testFirstName.value = firstName;
// 			const testLastName = document.createElement('input');
// 			testLastName.setAttribute("id", "lastName");
// 			testLastName.value = lastName;
// 			const testAge = document.createElement('input');
// 			testAge.setAttribute("id", "age");
// 			testAge.value = age;
// 			const testTable = document.createElement('table');
// 			testTable.setAttribute('id', "table");
// 			document.getElementById('table').append(testTable);
//
// 			saveData();
// 			const actual = localStorage.getItem('table');
//
// 			assert.deepEqual(actual, JSON.stringify(testTable));
// 		});
// 	});
//
//
// let sandbox;
//
// before(() => {
// 	sandbox = sinon.createSandbox();
// });
//
// afterEach(() => {
// 	sandbox.restore();
// });
// describe('addStart function', () => {
//
//
// 	// afterEach(() => {
// 	// 	sandbox.restore();
// 	// 	document.getElementById('table').innerHTML = null;
// 	// });
//
// 	const testData = [
// 		{
// 			id: '1',
// 			firstName: 'Leonid',
// 			lastName: 'Makarovich',
// 			age: '89',
// 		},
// 		{
// 			id: '2',
// 			firstName: 'Leonid',
// 			lastName: 'Danylovich',
// 			age: '81',
// 		},
// 		{
// 			id: '3',
// 			firstName: 'Victor',
// 			lastName: 'Andrijovych',
// 			age: '65',
// 		},
// 		{
// 			id: '4',
// 			firstName: 'Victor',
// 			lastName: 'Fedorovych',
// 			age: '69',
// 		},
// 		{
// 			id: '5',
// 			firstName: 'Petro',
// 			lastName: 'Oleksiyovych',
// 			age: '54',
// 		},
//
// 	];
//
// 	testData.forEach(data => {
//
// 		const {id, firstName, lastName, age} = data;
//
// 		it('function getData called once', () => {
// 			const testId = document.createElement('input');
// 			testId.setAttribute("id", "id");
// 			testId.value = id;
// 			const testFirstName = document.createElement('input');
// 			testFirstName.setAttribute("id", "firstName");
// 			testFirstName.value = firstName;
// 			const testLastName = document.createElement('input');
// 			testLastName.setAttribute("id", "lastName");
// 			testLastName.value = lastName;
// 			const testAge = document.createElement('input');
// 			testAge.setAttribute("id", "age");
// 			testAge.value = age;
// 			const testTable = document.createElement('table');
// 			testTable.setAttribute('id', "table");
// 			document.getElementById('table').append(testTable);
//
// 			// const stub = sandbox.stub(window, 'getData');
//
// 			addStart();
// 			assert.deepEqual()
// 			// sandbox.assert.calledOnce(stub);
// 		});
//
// 		it('function alert called once', () => {
// 			const testId = document.createElement('input');
// 			testId.setAttribute("id", "id");
// 			const testFirstName = document.createElement('input');
// 			testFirstName.setAttribute("id", "firstName");
// 			const testLastName = document.createElement('input');
// 			testLastName.setAttribute("id", "lastName");
// 			const testAge = document.createElement('input');
// 			testAge.setAttribute("id", "age");
// 			const testTable = document.createElement('table');
// 			testTable.setAttribute('id', "table");
// 			document.getElementById('table').append(testTable);
// 			testId.value = '';
// 			testFirstName.value = 'Leonid';
// 			testLastName.value = 'Makarovych';
// 			testAge.value = '89';
// 			const stub = sandbox.stub(window, 'alert');
//
// 			addStart();
//
// 			sandbox.assert.calledOnce(stub);
// 			// sandbox.assert.calledWith(stub, 'Input all data, please!');
// 		});
// 	});
// });

// describe('test setBegin', () => {
//
//     it('function setBegin calledOnce function beginSquare', () => {
//         const stub = sandbox.stub(window, 'beginSquare');
//         buttonType = 'square';
//
//         setBegin(buttonType);
//
//         sandbox.assert.calledOnce(stub);
//     });
//
//     it('function setBegin calledOnce function beginCircle', () => {
//         const stub = sandbox.stub(window, 'beginCircle');
//         buttonType = 'oval';
//
//         setBegin(buttonType);
//
//         sandbox.assert.calledOnce(stub);
//     });
//
//     it('function setBegin calledOnce function beginCurve', () => {
//         const stub = sandbox.stub(window, 'beginCurve');
//         buttonType = 'curve';
//
//         setBegin(buttonType);
//
//         sandbox.assert.calledOnce(stub);
//     });
//
//     it('function setBegin calledOnce function beginLine', () => {
//         const stub = sandbox.stub(window, 'beginLine');
//         buttonType = 'line';
//
//         setBegin(buttonType);
//
//         sandbox.assert.calledOnce(stub);
//     });
// });
//
// describe('test setEnd', () => {
//
//     it('function setEnd calledOnce function endSquare', () => {
//         const stub = sandbox.stub(window, 'endSquare');
//         buttonType = 'square';
//
//         setEnd(buttonType);
//
//         sandbox.assert.calledOnce(stub);
//         sandbox.assert.calledWith(stub, 'square');
//     });
//
//     it('function setEnd calledOnce function endCircle', () => {
//         const stub = sandbox.stub(window, 'endCircle');
//         buttonType = 'oval';
//
//         setEnd(buttonType);
//
//         sandbox.assert.calledOnce(stub);
//         sandbox.assert.calledWith(stub, 'oval');
//     });
//
//     it('function setEnd calledOnce function endCurve', () => {
//         const stub = sandbox.stub(window, 'endCurve');
//         buttonType = 'curve';
//
//         setEnd(buttonType);
//
//         sandbox.assert.calledOnce(stub);
//         sandbox.assert.calledWith(stub, 'curve');
//     });
//
//     it('function setEnd calledOnce function endLine', () => {
//         const stub = sandbox.stub(window, 'endLine');
//         buttonType = 'line';
//
//         setEnd(buttonType);
//
//         sandbox.assert.calledOnce(stub);
//         sandbox.assert.calledWith(stub, 'line');
//     });
// });

