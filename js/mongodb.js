const mysql = require('mysql');
const mySQLConfig = {
	host: 'localhost',
	user: 'root',
	password:'',
	database: 'personsdb',
};
const connection = mysql.createPool(mySQLConfig);
connection.connect();
console.log('client is running')

// const http = require('http');
//
// const server = http.createServer();
//
// let dataArray = [
// 	{
// 		id: '1',
// 		firstName: 'Leonid',
// 		lastName: 'Makarovich',
// 		age: '89',
// 	},
// 	{
// 		id: '2',
// 		firstName: 'Leonid',
// 		lastName: 'Danylovich',
// 		age: '81',
// 	},
// 	{
// 		id: '3',
// 		firstName: 'Victor',
// 		lastName: 'Andrijovych',
// 		age: '65',
// 	},
// 	{
// 		id: '4',
// 		firstName: 'Victor',
// 		lastName: 'Fedorovych',
// 		age: '69',
// 	},
// 	{
// 		id: '5',
// 		firstName: 'Petro',
// 		lastName: 'Oleksiyovych',
// 		age: '54',
// 	},
// ];
//
// const html ='<div>html</div>';
// const css = '.container {\n' +
// 	'  display: flex;\n' +
// 	'  flex-flow: row wrap;\n' +
// 	'}';
// const js = JSON.stringify(dataArray);
//
// 	server.on('request', (request, response) => {
// 		request.setEncoding('utf8');
// 		console.log(request.method);
//
// 		if (request.method === 'POST') {
// 			request.on('data', (chunk) => {
// 			console.log(chunk);
// 			const requestData = JSON.parse(chunk);
// 			console.log(requestData);
// 				switch (requestData.action) {
// 					case 'onloadGet':
// 						const responseData = {
// 							version: requestData.version,
// 							action: requestData.action,
// 							dataArray: dataArray,
// 						};
// 						sendResponse(response, responseData);
// 						break;
// 					case 'create':
// 						if (checkId(requestData.id, dataArray)) {
// 							console.log(checkId(requestData.id, dataArray));
// 							const person = {
// 								id: requestData.id,
// 								firstName: requestData.firstName,
// 								lastName: requestData.lastName,
// 								age: requestData.age,
// 							};
// 							dataArray.push(person);
// 							const responseData = {
// 								version: requestData.version,
// 								action: requestData.action,
// 								person: person,
// 							};
// 							sendResponse(response, responseData);
// 						} else {
// 							sendResponse(response, 'Id is already exist!');
// 						}
// 						break;
// 					case 'update':
// 						if (checkId(requestData.id, dataArray)) {
// 							sendResponse(response, 'Id is not exist!');
// 						} else {
// 							const person = {
// 								id: requestData.id,
// 								firstName: requestData.firstName,
// 								lastName: requestData.lastName,
// 								age: requestData.age,
// 							};
//
// 							for (let index = 0; index < dataArray.length; index++) {
// 								if (person.id === dataArray[index].id) {
// 									dataArray[index].firstName = person.firstName;
// 									dataArray[index].lastName = person.lastName;
// 									dataArray[index].age = person.age;
// 								}
// 							}
// 							const responseData = {
// 								version: requestData.version,
// 								action: requestData.action,
// 								person: person,
// 							};
// 							sendResponse(response, responseData);
// 						}
//
// 					case 'delete':
// 						if (checkId(requestData.id, dataArray)) {
// 							sendResponse(response, 'Id is not exist!');
// 						} else {
// 							const person = {
// 								id: requestData.id,
// 								firstName: requestData.firstName,
// 								lastName: requestData.lastName,
// 								age: requestData.age,
// 							};
//
// 							for (let index = 0; index < dataArray.length; index++) {
// 								if (person.id === dataArray[index].id) {
// 									dataArray.splice(index, 1);
// 								}
// 							}
// 							const responseData = {
// 								version: requestData.version,
// 								action: requestData.action,
// 								person: person,
// 							};
// 							sendResponse(response, responseData);
// 						}
// 					// default:
// 					// 	response.writeHead(404, {'Content-Type': 'text/plain'});
// 					// 	response.end('404 not found');
// 				}
//
// 			});
// 		} else {
// 			sendResponse(response, dataArray);
// 		}
// 	});
//
//
// server.listen(3000, 'localhost', () => console.log('Server is working!'));
//
// function dataPush(newItem, dataArray) {
// 	dataArray.push(newItem);
// 	return dataArray;
// }
// function create(dataArray, table) {
// 	const newData = getData(id, firstName, lastName, age);
// 	if (checkId(newData.id, dataArray)) {
// 		dataArray.push(newData);
// 		table.append(createRow(newData));
// 		return dataArray;
// 	} else {
// 		alert('Id is already exist!')
// 	}
// }
//
// function checkId(id, dataArray) {
// 	let isIdExist = 0;
// 	if (typeof (Number(id)) === 'number') {
// 		dataArray.forEach(dataObject => {
// 				if (Number(dataObject.id) === Number(id)) {
// 					isIdExist = 1;
// 				}
// 			}
// 		);
// 		if (!isIdExist) {
// 			return true;
// 		} else return false;
// 	} else return false
// }
//
// function sendResponse(response, data) {
// 	response.setHeader('Access-Control-Allow-Origin', '*');
// 	response.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
// 	response.setHeader('Access-Control-Allow-Headers', 'Content-Type');
// 	response.writeHead(200, {'Content-Type': 'text/json'});
// 	response.end(JSON.stringify(data));
// }
//
//
// // console.log(response);
// // console.log(response.method);
// // console.log(response.headers);
//
